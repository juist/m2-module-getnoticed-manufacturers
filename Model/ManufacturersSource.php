<?php

namespace GetNoticed\Manufacturers\Model;

use GetNoticed\Manufacturers\Model\ResourceModel\Manufacturer\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;

class ManufacturersSource implements ArrayInterface
{

    /**
     * @var CollectionFactory
     */
    protected $collection;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collection = $collectionFactory->create();
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $items = [];

        foreach ($this->collection as $item) {
            $items[] = [
                'value' => $item->getId(),
                'label' => $item->getName()
            ];
        }

        return $items;
    }

}
