<?php

namespace GetNoticed\Manufacturers\Model;

use Magento\Backend\Block\AbstractBlock;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use GetNoticed\Manufacturers\Model\ResourceModel\Manufacturer as ResourceManufacturer;
/**
 * Class Manufacturer
 *
 * @package GetNoticed\Manufacturers\Model
 */
class Manufacturer extends AbstractModel implements ManufacturerInterface
{

    /**
     * @var int
     */
    const FIELD_OPTION_ID = 'option_id';

    /**
     * @var string
     */
    const FIELD_NAME = 'name';

    /**
     * @var string
     */
    const FIELD_SLUG = 'slug';

    /**
     * @var string
     */
    const FIELD_DESCRIPTION = 'description';

    /**
     * @var string
     */
    const FIELD_SEO_DESCRIPTION = 'seo_description';

    /**
     * @var string
     */
    const FIELD_LOGO = 'logo';

    /**
     * @var string
     */
    const URL_REWRITE_ENTITY = 'manufacturer';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * Manufacturer constructor.
     *
     * @param StoreManagerInterface                                        $storeManager
     * @param UrlInterface                                                 $url
     * @param \Magento\Framework\Model\Context                             $context
     * @param \Magento\Framework\Registry                                  $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null           $resourceCollection
     * @param array                                                        $data
     */
    public function __construct(StoreManagerInterface $storeManager,
        UrlInterface $url,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->url = $url;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(ResourceManufacturer::class);
    }

    /**
     * @return int
     */
    public function getOptionId()
    {
        return $this->getData(self::FIELD_OPTION_ID);
    }

    /**
     * @param int $optionId
     * @return $this
     */
    public function setOptionId($optionId)
    {
        return $this->setData(self::FIELD_OPTION_ID, $optionId);
    }

    /**
     * @return string
     */
    public function getName()
    {
        if (!$this->hasData(self::FIELD_NAME) || null === $this->getData(self::FIELD_NAME)) {
            $this->setData(self::FIELD_NAME, $this->getResource()->getManufacturerName($this));
        }

        return $this->getData(self::FIELD_NAME);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url->getUrl($this->getSlug());
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->getData(self::FIELD_SLUG);
    }

    /**
     * @param $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        return $this->setData(self::FIELD_SLUG, $slug);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::FIELD_DESCRIPTION);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData(self::FIELD_DESCRIPTION, $description);
    }

    /**
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->getData(self::FIELD_SEO_DESCRIPTION);
    }

    /**
     * @param string $seoDescription
     * @return $this
     */
    public function setSeoDescription($seoDescription)
    {
        return $this->setData(self::FIELD_SEO_DESCRIPTION, $seoDescription);
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->getData(self::FIELD_LOGO);
    }

    /**
     * @return null|string
     */
    public function getLogoUrl()
    {
        if ($this->getLogo()) {
            return $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . $this->getLogo();
        }

        return null;
    }

    /**
     * @param string $logoPath
     * @return $this
     */
    public function setLogo($logoPath)
    {
        return $this->setData(self::FIELD_LOGO);
    }

}
