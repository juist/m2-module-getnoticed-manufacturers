<?php

namespace GetNoticed\Manufacturers\Model;

interface ManufacturerInterface
{

    public function getOptionId();

    public function setOptionId($optionId);

    public function getName();

    public function getUrl();

    public function getSlug();

    public function setSlug($slug);

    public function getDescription();

    public function setDescription($description);

    public function getSeoDescription();

    public function setSeoDescription($seoDescription);

    public function getLogo();

    public function setLogo($logoPath);

}
