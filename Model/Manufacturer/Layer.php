<?php

namespace GetNoticed\Manufacturers\Model\Manufacturer;

use GetNoticed\Manufacturers\Controller\Manufacturers\Index;
use GetNoticed\Manufacturers\Model\Manufacturer;
use GetNoticed\Manufacturers\Model\ManufacturerFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as CollectionFactory;
use Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection as FulltextCollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\Layer\ContextInterface;
use Magento\Catalog\Model\Layer\StateFactory ;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Registry;

/**
 * Class Layer
 *
 * @package GetNoticed\Manufacturers\Model\Manufacturer
 */
class Layer extends \Magento\Catalog\Model\Layer
{

    /**
     * @var FulltextCollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ManufacturerFactory
     */
    protected $manufacturerFactory;

    /**
     * @var CollectionFactory
     */
    protected $originalCollectionFactory;

    /**
     * Layer constructor.
     *
     * @param ManufacturerFactory         $manufacturerFactory
     * @param ContextInterface            $context
     * @param StateFactory                $layerStateFactory
     * @param AttributeCollectionFactory  $attributeCollectionFactory
     * @param Product                     $catalogProduct
     * @param StoreManagerInterface       $storeManager
     * @param Registry                    $registry
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CollectionFactory           $collectionFactory
     * @param array                       $data
     */
    public function __construct(
        ManufacturerFactory $manufacturerFactory,
        ContextInterface $context,
        StateFactory $layerStateFactory,
        AttributeCollectionFactory $attributeCollectionFactory,
        Product $catalogProduct,
        StoreManagerInterface $storeManager,
        Registry $registry,
        CategoryRepositoryInterface $categoryRepository,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->manufacturerFactory = $manufacturerFactory;

        $fulltextCollectionFactory = ObjectManager::getInstance()->get(FulltextCollectionFactory::class);

        $this->_layerStateFactory = $layerStateFactory;
        $this->_attributeCollectionFactory = $attributeCollectionFactory;
        $this->_catalogProduct = $catalogProduct;
        $this->_storeManager = $storeManager;
        $this->registry = $registry;
        $this->categoryRepository = $categoryRepository;
        $this->collectionProvider = $context->getCollectionProvider();
        $this->stateKeyGenerator = $context->getStateKey();
        $this->collectionFilter = $context->getCollectionFilter();
        $this->collectionFactory = $fulltextCollectionFactory;
        $this->originalCollectionFactory = $collectionFactory;

        parent::__construct(
            $context,
            $layerStateFactory,
            $attributeCollectionFactory,
            $catalogProduct,
            $storeManager,
            $registry,
            $categoryRepository,
            $data
        );
    }

    /**
     * Retrieve current layer product collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollection()
    {
        if (!array_key_exists('default', $this->_productCollections)) {
            // Layer required a fulltext collection, otherwise it would fail on getFaceted functions
            // So @todo is to fix layered navigation for manufacturer page
            //$collection = $this->collectionFactory->create();

            /**
             * @var \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection $collection
             */
            $collection = $this->originalCollectionFactory->create();
            $collection
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('status', Status::STATUS_ENABLED)
                ->addAttributeToFilter('visibility', ['in' => [Visibility::VISIBILITY_BOTH, Visibility::VISIBILITY_IN_CATALOG]]);
            $this->prepareProductCollection($collection);

            $this->_productCollections['default'] = $collection;
        }

        return $this->_productCollections['default'];
    }

    /**
     * Initialize product collection
     *
     * @param  \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\Layer
     */
    public function prepareProductCollection($collection)
    {
        $collection
            ->addAttributeToFilter('manufacturer', $this->getCurrentManufacturer()->getOptionId());

        return $this;
    }


    /**
     * Retrieve current manufacturer model
     *
     * @return mixed
     * @throws LocalizedException
     */
    public function getCurrentManufacturer()
    {
        if ($this->registry->registry(Index::REGISTRY_KEY)) {
            return $this->registry->registry(Index::REGISTRY_KEY);
        } elseif ($this->registry->registry('current_product')) {
            /**
             * @var Product $product
             */
            $product = $this->registry->registry('current_product');
            $manufacturer = $this->manufacturerFactory->create()->getResource()->loadByOptionId($product->getManufacturer());

            $this->registry->register(Index::REGISTRY_KEY, $manufacturer);

            return $this->registry->registry(Index::REGISTRY_KEY);
        }

        throw new LocalizedException(__('Failed to initialize product'));
    }

}
