<?php

namespace GetNoticed\Manufacturers\Model\ResourceModel\Manufacturer;

use Magento\Framework\DB\Select;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use GetNoticed\Manufacturers\Model\Manufacturer;
use Magento\Store\Model\StoreManagerInterface;

/**
 * {@inheritdoc}
 */
class Collection extends AbstractCollection
{

    /**
     * @var bool
     */
    protected $_joinedOptionName = false;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Collection constructor.
     *
     * @param StoreManagerInterface                                        $storeManager
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface    $entityFactory
     * @param \Psr\Log\LoggerInterface                                     $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface                    $eventManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null          $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null    $resource
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->storeManager = $storeManager;

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }


    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(Manufacturer::class, \GetNoticed\Manufacturers\Model\ResourceModel\Manufacturer::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getSelect()
    {
        $select = parent::getSelect();

        if (false === $this->_joinedOptionName) {
            $select
                ->joinLeft(
                    ['eav_attribute_option_value' => $this->getTable('eav_attribute_option_value')],
                    sprintf('main_table.option_id = eav_attribute_option_value.option_id AND eav_attribute_option_value.store_id = %d', $this->storeManager->getDefaultStoreView()->getId()),
                    ['name' => 'eav_attribute_option_value.value']
                );
            $this->_joinedOptionName = true;
        }

        // Get where part
        $where = $select->getPart(Select::WHERE);

        // If it's filtering name, change it to the proper name
        foreach ($where as $idx => $clause) {
            if (false !== stripos($clause, '`name` LIKE')) {
                $where[$idx] = strtr(
                    $clause, [
                    '`name`' => '`eav_attribute_option_value`.`value`'
                    ]
                );
            }
        }

        $select->setPart(Select::WHERE, $where);

        return $select;
    }

    /**
     * {@inheritdoc}
     */
    public function getSelectCountSql()
    {
        // Let Magento do some magic
        $countSelect = parent::getSelectCountSql();

        // Get where part
        $where = $countSelect->getPart(Select::WHERE);

        // If it's filtering name, change it to the proper name
        foreach ($where as $idx => $clause) {
            if (false !== stripos($clause, '`name` LIKE')) {
                $where[$idx] = strtr(
                    $clause, [
                    '`name`' => '`eav_attribute_option_value`.`value`'
                    ]
                );
            }
        }
        $countSelect->setPart(Select::WHERE, $where);

        return $countSelect;
    }

}
