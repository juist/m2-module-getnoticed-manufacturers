<?php

namespace GetNoticed\Manufacturers\Sync;

use GetNoticed\Manufacturers\Model\Manufacturer;
use GetNoticed\Manufacturers\Model\ManufacturerFactory;
use GetNoticed\Manufacturers\Model\ResourceModel\Manufacturer\CollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\AttributeFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filter\FilterManager;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;

/**
 * Class Manufacturers
 *
 * @package GetNoticed\Manufacturers\Sync
 */
class Manufacturers
{

    /**
     * @var string
     */
    const ATTRIBUTE_CODE = 'manufacturer';

    /**
     * @var AttributeFactory
     */
    protected $attributeFactory;

    /**
     * @var ManufacturerFactory
     */
    protected $manufacturerFactory;

    /**
     * @var CollectionFactory
     */
    protected $manufacturerCollectionFactory;

    /**
     * @var UrlRewriteCollectionFactory
     */
    protected $urlRewriteCollectionFactory;

    /**
     * @var FilterManager
     */
    protected $filterManager;


    /**
     * Sync constructor.
     * Manufacturers constructor.
     *
     * @param AttributeFactory    $attributeFactory
     * @param ManufacturerFactory $manufacturerFactory
     * @param CollectionFactory   $manufacturerCollectionFactory
     * @param FilterManager       $filterManager
     */
    public function __construct(AttributeFactory $attributeFactory,
        ManufacturerFactory $manufacturerFactory,
        CollectionFactory $manufacturerCollectionFactory,
        UrlRewriteCollectionFactory $urlRewriteCollectionFactory,
        FilterManager $filterManager
    ) {
        $this->attributeFactory = $attributeFactory;
        $this->manufacturerFactory = $manufacturerFactory;
        $this->manufacturerCollectionFactory = $manufacturerCollectionFactory;
        $this->urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
        $this->filterManager = $filterManager;
    }

    /**
     * Synchronize manufacturer options with the GetNoticed manufacturers
     */
    public function sync()
    {
        $this->deleteManufacturersWithoutIds($this->manufacturerCollectionFactory->create()->getAllIds());

        // Add missing manufacturers
        foreach ($this->getAttribute()->getSource()->getAllOptions() as $option) {
            if ($option['value']) {
                /**
                 * @var Manufacturer $manufacturer
                 */
                try {
                    $manufacturer = $this->manufacturerFactory->create();
                    $manufacturer = $manufacturer->getResource()->loadByOptionId($option['value']);
                } catch (LocalizedException $e) {
                    $manufacturer = null;
                }

                if ((is_null($manufacturer) || !$manufacturer instanceof Manufacturer || !$manufacturer->getId()) && (int)$option['value'] > 0) {
                    $manufacturer = $this->manufacturerFactory->create();
                    $manufacturer
                        ->setOptionId((int)$option['value'])
                        ->setDescription('')
                        ->setSeoDescription('')
                        ->setLogo('');

                    $manufacturer->getResource()->save($manufacturer);
                }
            }
        }

        // Load manufacturers with mising data
        $manufacturers = $this->manufacturerCollectionFactory->create();
        $manufacturers
            ->addFieldToSelect('*')
            ->addFieldToFilter(
                'slug', [
                ['eq' => ''],
                ['null' => true]
                ]
            );

        foreach ($manufacturers as $manufacturer) {
            if ($manufacturer->getName()) {
                try {
                    $manufacturer->setSlug($this->filterManager->translitUrl($manufacturer->getName()));
                    $manufacturer->getResource()->save($manufacturer);
                } catch (\Exception $e) {
                    throw new \Exception(sprintf('Could not save manufacturer %d: ' . $e->getMessage(), $manufacturer->getId()));
                }
            }
        }

        // Return success status
        return true;
    }

    /**
     * @return \Magento\Eav\Model\Entity\Attribute
     * @throws LocalizedException
     */
    protected function getAttribute()
    {
        $attribute = $this->attributeFactory->create();
        $attribute->loadByCode(Product::ENTITY, self::ATTRIBUTE_CODE);

        if (!$attribute->getId()) {
            throw new LocalizedException(__('Failed to load attribute'));
        }

        return $attribute;
    }


    /**
     * @param $manufacturerIds
     */
    private function deleteManufacturersWithoutIds($manufacturerIds)
    {
        $urlRewriteCollection = $this->urlRewriteCollectionFactory->create();
        $urlRewriteCollection->addFieldToFilter('entity_type', ['eq' => 'manufacturer'])
            ->addFieldToFilter('entity_id', ['nin' => $manufacturerIds])
            ->walk('delete');
    }

}
