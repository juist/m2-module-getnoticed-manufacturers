<?php

namespace GetNoticed\Manufacturers\Helper;


use Magento\Framework\App\Helper\AbstractHelper;

class SecurityHelper extends AbstractHelper
{

    const PATTERN_CIDR_IP_ADDRESS = '#^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$#';

    /**
     * Generates a random secure string
     *
     * @param  $lengthOfToken
     * @param  null          $chars
     * @return string
     */
    public function generateSecureToken($lengthOfToken, $chars = null)
    {
        return bin2hex(random_bytes($lengthOfToken));
    }

    /**
     * @return \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    public function getVisitorRemoteAddress()
    {
        return $this->_remoteAddress;
    }

    /**
     * @return string
     */
    public function getVisitorIpAddress()
    {
        return $this->getVisitorRemoteAddress()->getRemoteAddress();
    }

    public function createSalt($string)
    {
        return hash('sha512', $string);
    }

    public function convertIpAddressSettingToArray($setting)
    {
        $ipAddresses = explode(',', $setting);

        foreach ($ipAddresses as $k => &$ipAddress) {
            $ipAddress = trim($ipAddress);

            if (preg_match(self::PATTERN_CIDR_IP_ADDRESS, $ipAddress) < 1) {
                unset($ipAddresses[$k]);
            }
        }

        return $ipAddresses;
    }




    /**
     * @param null|string $ipAddresses The IP addresses to check against $cidrRange (current IP is used if null)
     * @param array       $cidrRange   An IP of cidr-notated IP ranges (e.g. 192.168.0.0/24)
     * @return bool
     */
    public function checkIp($ipAddresses, array $cidrRange)
    {
        if (is_null($ipAddresses)) {
            $ipAddresses = [$this->getVisitorIpAddress()];
        }

        if (!is_array($ipAddresses)) {
            $ipAddresses = [$ipAddresses];
        }

        foreach ($ipAddresses as $ipAddress) {
            foreach ($cidrRange as $cidrIp) {
                if ($this->cidrCheck($ipAddress, $cidrIp)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Checks if $ip is in $cidr range
     *
     * @param  string $ip
     * @param  string $cidr
     * @return bool
     */
    public function cidrCheck($ip, $cidr)
    {
        list($net, $mask) = explode('/', $cidr);

        $ipNet = ip2long($net);
        $ipMask = ~((1 << (32 - $mask)) - 1);

        $ipAddress = ip2long($ip);
        $ipAddressNet = $ipAddress & $ipMask;

        return ($ipAddressNet == $ipNet);
    }

}
