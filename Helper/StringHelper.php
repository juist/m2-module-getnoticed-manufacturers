<?php

namespace GetNoticed\Manufacturers\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\LocalizedException;

class StringHelper extends AbstractHelper
{

    const FILTER_MODE_REMOVE_CHARS = 1;
    const FILTER_MODE_ALLOW_CHARS = 2;


    /**
     * @param $str
     * @param $chars
     * @param $mode
     * @return string
     * @throws LocalizedException
     */
    public function filter($str, $chars, $mode)
    {
        if ($mode & self::FILTER_MODE_REMOVE_CHARS) {
            // Remove $chars from $str
            $remove = [];

            foreach ($chars as $k => $char) {
                if (is_int($k)) {
                    $remove[$char] = '';
                } else {
                    $remove[$k] = $char;
                }
            }

            return strtr($str, $remove);
        } else if ($mode & self::FILTER_MODE_ALLOW_CHARS) {
            // Remove all but $chars from $str
            $returnStr = '';
            foreach ($this->mb_str_split($str) as $strChar) {
                if (in_array($strChar, $chars, true)) {
                    $returnStr .= $strChar;
                }
            }

            return $returnStr;
        } else {
            throw new LocalizedException(__('No valid mode'));
        }
    }

    /**
     * Multibyte version of str_split
     *
     * @param  $str
     * @param  string $encoding
     * @return array
     */
    public function mb_str_split($str, $encoding = "UTF-8") 
    {
        $strLen = mb_strlen($str);
        $array = [];

        while ($strLen) {
            $array[] = mb_substr($str, 0, 1, $encoding);
            $str = mb_substr($str, 1, $strLen, $encoding);
            $strLen = mb_strlen($str);
        }

        return $array;
    }

}
