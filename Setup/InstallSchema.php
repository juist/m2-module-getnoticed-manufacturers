<?php

namespace GetNoticed\Manufacturers\Setup;

use GetNoticed\Manufacturers\Setup\AbstractInstallSchema;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema extends AbstractInstallSchema
{

    const TABLE_GETNOTICED_MANUFACTURERS = self::TABLE_PREFIX . '_manufacturers';

    /**
     * @var SchemaSetupInterface
     */
    protected $schemaSetup;

    /**
     * @var ModuleContextInterface
     */
    protected $moduleContext;

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // DI
        $this->schemaSetup = $setup;
        $this->moduleContext = $context;

        // Start setup
        $setup->startSetup();

        // Add tables
        $this->installTables();

        // End setup
        $setup->endSetup();
    }

    protected function installTables()
    {
        $tableName = $this->schemaSetup->getTable(self::TABLE_GETNOTICED_MANUFACTURERS);
        $eavAttributeOptionTable = $this->schemaSetup->getTable('eav_attribute_option');

        $table = $this->schemaSetup->getConnection()->newTable($tableName)
            ->addColumn(
                'manufacturer_id', Table::TYPE_INTEGER, null, [
                'unsigned' => true,
                'nullable' => false,
                'identity' => true,
                'primary'  => true
                ], 'Manufacturer Id'
            )
            ->addColumn(
                'option_id', Table::TYPE_INTEGER, null, [
                'unsigned' => true,
                'nullable' => false
                ], 'Option Id'
            )
            ->addForeignKey(
                $this->schemaSetup->getFkName($tableName, 'option_id', $eavAttributeOptionTable, 'option_id'), 'option_id',
                $eavAttributeOptionTable, 'option_id',
                Table::ACTION_CASCADE
            )
            ->addColumn(
                'slug', Table::TYPE_TEXT, null, [
                'nullable' => false
                ], 'Slug'
            )
            ->addColumn(
                'description', Table::TYPE_TEXT, null, [
                'nullable' => false
                ], 'Short Description'
            )
            ->addColumn(
                'seo_description', Table::TYPE_TEXT, null, [
                'nullable' => false
                ], 'SEO Description'
            )
            ->addColumn(
                'logo', Table::TYPE_TEXT, 255, [
                'nullable' => false
                ], 'Manufacturer logo'
            )
            ->setComment('Manufucturers table, getnoticed_manufacturer.option_id is 1-to-1 to eav_attribute_option.option_id');
        $this->schemaSetup->getConnection()->createTable($table);
    }

}
