<?php

namespace GetNoticed\Manufacturers\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Manufacturers module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addManufacturersPrimaryKey($setup);
            $this->addManufacturersSlug($setup);
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addUniqueIndexOptionId($setup);
        }

        $setup->endSetup();
    }

    protected function addManufacturersPrimaryKey(SchemaSetupInterface $setup)
    {
        $setup->getConnection()
            ->addColumn(
                $setup->getTable(InstallSchema::TABLE_GETNOTICED_MANUFACTURERS), 'manufacturer_id', [
                'type'     => Table::TYPE_INTEGER,
                'unsigned' => true,
                'nullable' => false,
                'identity' => true,
                'primary'  => true,
                'comment'  => 'Manufacturer Id'
                ]
            );
    }

    protected function addManufacturersSlug(SchemaSetupInterface $setup)
    {
        $setup->getConnection()
            ->addColumn(
                $setup->getTable(InstallSchema::TABLE_GETNOTICED_MANUFACTURERS), 'slug', [
                'type'     => Table::TYPE_TEXT,
                'nullable' => false,
                'comment'  => 'Slug'
                ]
            );
    }

    protected function addUniqueIndexOptionId(SchemaSetupInterface $setup)
    {
        $setup->getConnection()
            ->addIndex(
                $setup->getTable(InstallSchema::TABLE_GETNOTICED_MANUFACTURERS),
                $setup->getIdxName(
                    $setup->getTable(InstallSchema::TABLE_GETNOTICED_MANUFACTURERS),
                    ['option_id'],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['option_id'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            );
    }

}
