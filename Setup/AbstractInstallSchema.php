<?php

namespace GetNoticed\Manufacturers\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;

abstract class AbstractInstallSchema implements InstallSchemaInterface
{

    const TABLE_PREFIX = 'getnoticed';

}
