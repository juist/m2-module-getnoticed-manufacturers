# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
[GET-195](https://getnoticedbv.atlassian.net/browse/GET-195) - Updated composer name and version

## [1.4.3] - 2017-08-18
### Added
[GET-193](https://getnoticedbv.atlassian.net/browse/GET-193) - Deletes obsolete url rewrites when synchronizing for new manufacturers.
