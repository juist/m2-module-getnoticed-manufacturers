<?php

namespace GetNoticed\Manufacturers\Block;

use GetNoticed\Manufacturers\Controller\Manufacturers\Index;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class Manufacturer extends Template
{

    /**
     * @var \GetNoticed\Manufacturers\Model\Manufacturer
     */
    protected $manufacturer;

    /**
     * @var Registry
     */
    protected $registry;

    public function __construct(
        Template\Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->registry = $registry;

        parent::__construct($context, $data);
    }

    /**
     * @return \GetNoticed\Manufacturers\Model\Manufacturer
     */
    protected function getManufacturer()
    {
        return $this->registry->registry(Index::REGISTRY_KEY);
    }

    public function getPageTitle()
    {
        return $this->getManufacturer()->getName();
    }

    public function getDescription()
    {
        return $this->getManufacturer()->getDescription();
    }

    public function getSeoDescription()
    {
        return $this->getManufacturer()->getSeoDescription();
    }

}
