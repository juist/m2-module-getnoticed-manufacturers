<?php

namespace GetNoticed\Manufacturers\Block\Adminhtml\Grid\Edit;

use GetNoticed\Manufacturers\Controller\Adminhtml\Manufacturers\Edit;
use GetNoticed\Manufacturers\Model\Manufacturer;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Cms\Model\Wysiwyg\Config;

class Form extends Generic
{

    /**
     * @var Manufacturer
     */
    protected $manufacturer;

    /**
     * @var Config
     */
    protected $wysiwygConfig;

    public function __construct(
        Config $wysiwygConfig,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        $this->wysiwygConfig = $wysiwygConfig;

        parent::__construct($context, $registry, $formFactory, $data);
    }


    protected function _construct()
    {
        parent::_construct();

        $this->setId('grid_form');
        $this->setTitle(__('Manufacturer'));
    }

    protected function _prepareForm()
    {
        $manufacturer = $this->getManufacturer();

        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id'      => 'edit_form',
                    'action'  => $this->getData('action'),
                    'method'  => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );
        $form->setHtmlIdPrefix('manufacturer_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('General information'), 'class' => 'fieldset-wide']);
        $fieldset->addField('manufacturer_id', 'hidden', ['name' => 'manufacturer_id']);
        $fieldset->addField('name', 'note', ['text' => $this->getManufacturer()->getName(), 'label' => __('Name')]);
        $fieldset->addField('slug', 'text', ['name' => 'slug', 'label' => __('Slug')]);
        $fieldset->addField('description', 'textarea', ['name' => 'description', 'label' => __('Description')]);
        $fieldset->addField('seo_description', 'editor', ['name' => 'seo_description', 'label' => __('SEO description'), 'config' => $this->wysiwygConfig->getConfig()]);
        $fieldset->addField('logo', 'image', ['name' => 'logo', 'label' => __('Logo')]);

        $form->setValues($manufacturer->getData());
        $form->setUseContainer(true);

        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getManufacturer()
    {
        if (is_null($this->manufacturer)) {
            $this->manufacturer = $this->_coreRegistry->registry(Edit::REGISTRY_KEY);
        }

        return $this->manufacturer;
    }

}
