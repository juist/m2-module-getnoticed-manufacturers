<?php

namespace GetNoticed\Manufacturers\Block\Adminhtml\Grid;

use GetNoticed\Manufacturers\Model\Manufacturer;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;
use Magento\Backend\Block\Widget\Form\Container;

class Edit extends Container
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var Manufacturer
     */
    protected $manufacturer;

    public function __construct(Context $context,
                                Registry $registry,
                                array $data = []
    )
    {
        $this->coreRegistry = $registry;

        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_objectId = 'manufacturer_id';
        $this->_blockGroup = 'GetNoticed_Manufacturers';
        $this->_controller = 'adminhtml_grid';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save manufacturer'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label'          => __('Save and continue edit'),
                'class'          => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form']
                    ]
                ]
            ]
        );
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
    }

    public function getHeaderText()
    {
        return __('Edit manufacturer: %1', $this->escapeHtml($this->getManufacturer()->getName()));
    }

    public function getManufacturer()
    {
        if (is_null($this->manufacturer)) {
            $this->manufacturer = $this->coreRegistry->registry(\GetNoticed\Manufacturers\Controller\Adminhtml\Manufacturers\Edit::REGISTRY_KEY);
        }

        return $this->manufacturer;
    }

    public function _getSaveAndContinueUrl()
    {
        $this->getUrl('*/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }

}
