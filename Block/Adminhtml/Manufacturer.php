<?php

namespace GetNoticed\Manufacturers\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container;

/**
 * Class Manufacturer
 *
 * @package GetNoticed\Manufacturers\Block\Adminhtml
 */
class Manufacturer extends Container
{

    /**
     * Set block settings
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_manufacturer';
        $this->_blockGroup = 'GetNoticed_Manufacturers';
        $this->_headerText = __('Manufacturers');
        $this->_addButtonLabel = __('Synchronize new manufacturers');

        parent::_construct();
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/sync');
    }

}
