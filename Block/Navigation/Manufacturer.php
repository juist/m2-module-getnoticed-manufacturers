<?php

namespace GetNoticed\Manufacturers\Block\Navigation;

use GetNoticed\Manufacturers\Model\Manufacturer\Layer;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Layer\FilterList;
use Magento\Catalog\Model\Layer\AvailabilityFlagInterface;
use \Magento\LayeredNavigation\Block\Navigation;

class Manufacturer extends Navigation
{

    public function __construct(
        Layer $catalogLayer,
        Context $context,
        Resolver $layerResolver,
        FilterList $filterList,
        AvailabilityFlagInterface $visibilityFlag,
        array $data = []
    ) {
        parent::__construct($context, $layerResolver, $filterList, $visibilityFlag, $data);
        $this->_catalogLayer = $catalogLayer;
    }

}
