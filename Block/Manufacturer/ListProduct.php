<?php

namespace GetNoticed\Manufacturers\Block\Manufacturer;

use GetNoticed\Manufacturers\Model\Manufacturer\Layer;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Url\Helper\Data;
use Magento\Catalog\Block\Product\ListProduct as CatalogListProduct;

class ListProduct extends CatalogListProduct
{

    public function __construct(Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        Layer $catalogLayer,
        array $data = []
    ) {
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);

        $this->_catalogLayer = $catalogLayer;
    }


    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {
            $layer = $this->getLayer();
            $this->_productCollection = $layer->getProductCollection();
        }

        return $this->_productCollection;
    }

    /**
     * @return Layer
     */
    public function getLayer()
    {
        return $this->_catalogLayer;
    }

}
