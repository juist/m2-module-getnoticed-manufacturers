<?php

namespace GetNoticed\Manufacturers\FontAwesome\Icons;

use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\Module\Dir\Reader as ModuleReader;
use Magento\Framework\Xml\Parser as XmlParser;

 /**
  * Class Source
  *
  * @package GetNoticed\Manufacturers\FontAwesome\Icons
  */
class Source implements ArrayInterface
{

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $moduleDirReader;

    /**
     * @var \Magento\Framework\Xml\Parser
     */
    private $xmlParser;


    /**
     * Source constructor.
     *
     * @param \Magento\Framework\Module\Dir\Reader $moduleDirReader
     * @param \Magento\Framework\Xml\Parser        $xmlParser
     */
    public function __construct(
        ModuleReader $moduleDirReader,
        XmlParser $xmlParser
    ) {
        $this->moduleDirReader = $moduleDirReader;
        $this->xmlParser = $xmlParser;
    }

    public function loadIconOptionsFromXml()
    {
        // Load from XML
        $filePath = $this->moduleDirReader->getModuleDir('etc', 'GetNoticed_Manufacturer') . '/fa-icons.xml';
        $parsedArray = $this->xmlParser->load($filePath)->xmlToArray();

        // Fix some parsing errors
        $iconOptions = [];
        foreach ($parsedArray['config']['_value']['icons']['icon'] ?: [] as $options) {
            if (!is_array($options)) {
                $options = [$options];

                foreach ($options as $option) {
                    $iconOptions[] = $option;
                }
            }
        }

        return $iconOptions;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $options = $this->loadIconOptionsFromXml();
        $optionArray = [];

        sort($options, SORT_STRING + SORT_NATURAL);

        foreach ($options as $iconClass) {
            $optionArray[] = [
                'value' => $iconClass,
                'label' => $iconClass
            ];
        }

        return $optionArray;
    }

}
