<?php

namespace GetNoticed\Manufacturers\Controller\Adminhtml\Manufacturers;

use GetNoticed\Manufacturers\Model\ManufacturerFactory;
use Magento\Backend\App\Action;
use Magento\Backend\Helper\Js;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Data\Form\Element\Image;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Uploader as Uploader;
use Magento\Framework\Filesystem;
use Magento\Framework\Filter\FilterManager;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\PageCache\Model\Cache\Type;
use GetNoticed\Manufacturers\Sync\Manufacturers as Manufacturers;

class Save extends Action
{

    /**
     * @var TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * @var Js
     */
    protected $jsHelper;

    /**
     * @var ManufacturerFactory
     */
    protected $manufacturerFactory;

    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var Image
     */
    protected $imageModel;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var FilterManager
     */
    protected $filterManager;

    /**
     * @var \GetNoticed\Manufacturers\Sync\Manufacturers
     */
    protected $syncManufacturers;


    /**
     * Save constructor.
     *
     * @param Action\Context                               $context
     * @param TypeListInterface                            $cacheTypeList
     * @param Js                                           $jsHelper
     * @param ManufacturerFactory                          $manufacturerFactory
     * @param UploaderFactory                              $uploaderFactory
     * @param Image                                        $imageModel
     * @param Filesystem                                   $filesystem
     * @param FilterManager                                $filterManager
     * @param \GetNoticed\Manufacturers\Sync\Manufacturers $syncManufacturers
     */
    public function __construct(Action\Context $context,
        TypeListInterface $cacheTypeList,
        Js $jsHelper,
        ManufacturerFactory $manufacturerFactory,
        UploaderFactory $uploaderFactory,
        Image $imageModel,
        Filesystem $filesystem,
        FilterManager $filterManager,
        Manufacturers $syncManufacturers
    ) {
        $this->cacheTypeList = $cacheTypeList;
        $this->jsHelper = $jsHelper;
        $this->manufacturerFactory = $manufacturerFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->imageModel = $imageModel;
        $this->filesystem = $filesystem;
        $this->filterManager = $filterManager;
        $this->syncManufacturers = $syncManufacturers;

        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /**
         * @var Redirect $resultRedirect
         */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $model = $this->manufacturerFactory->create();
            $model->getResource()->load($model, $this->getRequest()->getParam('manufacturer_id'));

            if (!$model->getId()) {
                throw new LocalizedException(__('Manufacturer not found'));
            }

            try {
                foreach (['logo'] as $image) {
                    $data[$image] = $this->processImage($image, isset($data[$image]) ? $data[$image] : null);
                }

                if (!empty($data['slug'])) {
                    $data['slug'] = $this->filterManager->translitUrl($data['slug']);
                } else {
                    $data['slug'] = $this->filterManager->translitUrl($model->getName());
                }

                $model->setData($data);
                $model->getResource()->save($model);

                $this->cacheTypeList->invalidate(Type::TYPE_IDENTIFIER);
                $this->messageManager->addSuccessMessage(__('Manufacturer has been saved.'));
                $this->_session->setFormData(false);

                $this->syncManufacturers->sync();

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['manufacturer_id' => $model->getId(), '_current' => true]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the manufacturer: %1', $e->getMessage()));
            }

            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath('*/*/edit', ['manufacturer_id' => $this->getRequest()->getParam('manufacturer_id')]);
        }
    }

    protected function processImage($fieldName, $data)
    {
        try {
            if (isset($data['delete'])) {
                return '';
            } else {
                $uploader = $this->uploaderFactory->create(['fileId' => $fieldName]);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                $result = $uploader->save($this->getDirectoryWrite()->getAbsolutePath());

                return $result['file'];
            }
        } catch (\Exception $e) {
            if ($e->getCode() != Uploader::TMP_NAME_EMPTY) {
                throw new \Exception($e->getMessage());
            } else {
                if (isset($data['value'])) {
                    return $data['value'];
                }
            }
        }

        return '';
    }

    /**
     * @return Filesystem\Directory\WriteInterface
     */
    protected function getDirectoryWrite()
    {
        return $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
    }

}
