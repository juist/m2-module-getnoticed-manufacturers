<?php

namespace GetNoticed\Manufacturers\Controller\Adminhtml\Manufacturers;

use Magento\Backend\App\Action;
use GetNoticed\Manufacturers\Sync\Manufacturers;

/**
 * Class Sync
 *
 * @package GetNoticed\Manufacturers\Controller\Adminhtml\Manufacturers
 */
class Sync extends Action
{

    /**
     * @var string
     */
    const ADMIN_RESOURCE = 'GetNoticed_Manufacturers::content_elements_manufacturers';

    /**
     * @var string
     */
    const ATTRIBUTE_CODE = 'manufacturer';

    /**
     * @var \GetNoticed\Manufacturers\Sync\Manufacturers
     */
    protected $syncManufacturers;

    /**
     * Sync constructor.
     *
     * @param \Magento\Backend\App\Action\Context          $context
     * @param \GetNoticed\Manufacturers\Sync\Manufacturers $syncManufacturers
     */
    public function __construct(Action\Context $context,
        Manufacturers $syncManufacturers
    ) {
        $this->syncManufacturers = $syncManufacturers;

        parent::__construct($context);
    }

    /**
     * Synchronize manufacturer options with the GetNoticed manufacturers
     */
    public function execute()
    {
        $this->syncManufacturers->sync();

        $this->_redirect('*/*/index');
    }

}
