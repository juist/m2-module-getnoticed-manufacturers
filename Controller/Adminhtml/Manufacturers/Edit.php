<?php

namespace GetNoticed\Manufacturers\Controller\Adminhtml\Manufacturers;

use GetNoticed\Manufacturers\Model\Manufacturer;
use GetNoticed\Manufacturers\Model\ManufacturerFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Action
{

    const ADMIN_RESOURCE = 'GetNoticed_Manufacturers::content_elements_manufacturers';

    const REGISTRY_KEY = 'manufacturer_model';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    /**
     * @var Manufacturer
     */
    protected $manufacturer;

    /**
     * @var ManufacturerFactory
     */
    protected $manufacturerFactory;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    public function __construct(Action\Context $context,
        PageFactory $resultPageFactory,
        ManufacturerFactory $manufacturerFactory,
        Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->manufacturerFactory = $manufacturerFactory;
        $this->coreRegistry = $registry;

        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $manufacturerId = (int)$this->getRequest()->getParam('manufacturer_id');
            $this->getManufacturer($manufacturerId);
        } catch (LocalizedException $e) {
            return $this->_redirect('*/*/index');
        }

        return $this->getPage();
    }

    protected function getPage()
    {
        if (is_null($this->resultPage)) {
            $this->resultPage = $this->resultPageFactory->create();
            $this->_setActiveMenu(self::ADMIN_RESOURCE);
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Edit manufacturer: %1', $this->manufacturer->getName()));
            $this->_addBreadcrumb(__('Manufacturers'), __('Manufacturers'));
            $this->_addBreadcrumb(__('Edit manufacturer'), __('Edit manufacturer'));
        }

        return $this->resultPage;
    }

    protected function getManufacturer($manufacturerId)
    {
        if (is_null($this->manufacturer)) {
            $this->manufacturer = $this->manufacturerFactory->create();
            $this->manufacturer->getResource()->load($this->manufacturer, $manufacturerId);

            if (!$this->manufacturer->getId()) {
                throw new LocalizedException(__('Manufacturer not found'));
            }

            $this->coreRegistry->register(self::REGISTRY_KEY, $this->manufacturer);
        }

        return $this->manufacturer;
    }

}
