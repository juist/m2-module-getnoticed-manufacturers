<?php

namespace GetNoticed\Manufacturers\Controller\Adminhtml\Manufacturers;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    const ADMIN_RESOURCE = 'GetNoticed_Manufacturers::content_elements_manufacturers';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;

    public function __construct(Action\Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        return $this->getPage();
    }

    protected function getPage()
    {
        if (is_null($this->resultPage)) {
            $this->resultPage = $this->resultPageFactory->create();
            $this->_setActiveMenu(self::ADMIN_RESOURCE);
            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Manufacturers'));
            $this->_addBreadcrumb(__('Manufacturers'), __('Manufacturers'));
        }

        return $this->resultPage;
    }

}
