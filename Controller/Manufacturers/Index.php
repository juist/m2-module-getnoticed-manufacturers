<?php

namespace GetNoticed\Manufacturers\Controller\Manufacturers;

use GetNoticed\Manufacturers\Model\ManufacturerFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    const ID_PARAM = 'manufacturer_id';
    const REGISTRY_KEY = 'manufacturer';

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var ManufacturerFactory
     */
    protected $manufacturerFactory;

    /**
     * @var Registry
     */
    protected $registry;

    public function __construct(Context $context,
        PageFactory $pageFactory,
        ManufacturerFactory $manufacturerFactory,
        Registry $registry
    ) {
    
        $this->pageFactory = $pageFactory;
        $this->manufacturerFactory = $manufacturerFactory;
        $this->registry = $registry;

        parent::__construct($context);
    }

    public function execute()
    {
        $manufacturerId = $this->getRequest()->getParam(self::ID_PARAM);
        $manufacturer = $this->_initializeManufacturer($manufacturerId);

        return $this->pageFactory->create();
    }

    protected function _initializeManufacturer($id)
    {
        $manufacturer = $this->manufacturerFactory->create();
        $manufacturer->getResource()->load($manufacturer, $id);

        if (!$manufacturer->getId()) {
            throw new NotFoundException(__('Could not find manufacturer'));
        }

        $this->registry->register(self::REGISTRY_KEY, $manufacturer);

        return $manufacturer;
    }

}
